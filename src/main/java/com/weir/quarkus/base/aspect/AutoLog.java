package com.weir.quarkus.base.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.interceptor.InterceptorBinding;

/**
 * 接口调用日志注解
 * @author weir
 *
 */
@InterceptorBinding //这个就是标识这个注解作为拦截注解
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AutoLog {

}
