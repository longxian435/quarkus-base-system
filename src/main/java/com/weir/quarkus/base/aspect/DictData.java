package com.weir.quarkus.base.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字典注解
 * @author weir
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DictData {
	String code() default "";

    String text() default "";

    String table() default "";
    /**
     * 关联表(c.id  so.customer_id)
     * @return
     */
    String relationalTable() default "";
}
