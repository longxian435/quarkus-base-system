package com.weir.quarkus.base.system.entity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * 
 * @ClassName: SysUserRole
 * @Description: 后台用户角色中间实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Entity
@Table(name = "sys_user_role")
public class SysUserRole extends BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	@Column(name = "user_id")
	public Integer userId;
	@Column(name = "role_id")
	public Integer roleId;
	public Integer getRoleId() {
		return roleId;
	}
	public SysUserRole(Integer userId, Integer roleId) {
		super();
		this.userId = userId;
		this.roleId = roleId;
	}
	public SysUserRole() {
		super();
	}
	

}