package com.weir.quarkus.base.system.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;

/**
 * 字典
 * @author weir
 *
 */
@Entity
@Table(name = "sys_dict")
public class SysDict extends BaseEntity {
	private static final long serialVersionUID = -7926845745350235387L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	@Column(unique = true)
	@NotBlank
    public String name;
	
	@Column(unique = true)
	@NotBlank
    public String code;

	public Integer getId() {
		return id;
	}
}