package com.weir.quarkus.base.system.vo;

public class PermissionsVo {

	public String label;
	public String value;
	public PermissionsVo(String label, String value) {
		super();
		this.label = label;
		this.value = value;
	}
	public PermissionsVo() {
		super();
	}
	
}
