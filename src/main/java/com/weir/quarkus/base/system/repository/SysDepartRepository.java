package com.weir.quarkus.base.system.repository;

import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;

import com.weir.quarkus.base.system.entity.SysDepart;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class SysDepartRepository implements PanacheRepository<SysDepart> {

}
