package com.weir.quarkus.base.system.resource;

import com.weir.quarkus.base.system.entity.SysDepart;
import com.weir.quarkus.base.system.mapper.SysDepartMapper;
import com.weir.quarkus.base.system.vo.SysDepartVo;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

@Tag(name = "部门管理")
@Path("/depart")
@ApplicationScoped
public class SysDepartResource {
	
	@Inject
	SysDepartMapper sysDepartMapper;
	
	@GET
	@Path("/{id}")
	public SysDepart get(@PathParam("id") Integer id) {
		return SysDepart.findById(id);
	}
	@GET
	@Path("list-all")
	public List<SysDepart> listAll() {
		return SysDepart.listAll();
	}

	@POST
	@Transactional
	public SysDepart add(SysDepart depart) {
		depart.persist();
		return depart;
	}
	@PUT
	@Transactional
	public Response edit(SysDepartVo departVo) {
		SysDepart depart = SysDepart.findById(departVo.getId());
		sysDepartMapper.merge(depart, departVo);
		
		return Response.ok(sysDepartMapper.toDTO(depart)).build();
	}

}
