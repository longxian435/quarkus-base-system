package com.weir.quarkus.base.system.vo;

import java.util.Map;

/**
 * 
 * @ClassName: MenuVo
 * @Description: easyui树形格式
 * @author weir
 * @date 2021年8月26日
 *
 */
public class MenuVo {

	public Integer id;
	public String text;
	public String name;
	public String code;
	public String iconcls;
	public String url;

	public String state;
	public Integer parentId;
	public String parentName;
	public Map<String, Object> attributes;
}