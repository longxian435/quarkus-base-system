package com.weir.quarkus.base.system.vo;

public class TokenVo {

	public String token;

	public TokenVo() {
		super();
	}

	public TokenVo(String token) {
		super();
		this.token = token;
	}
}
