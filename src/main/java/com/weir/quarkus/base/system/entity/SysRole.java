package com.weir.quarkus.base.system.entity;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotBlank;

/**
 * 
 * @ClassName: SysRole
 * @Description: 后台角色实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Entity
@Table(name = "sys_role")
public class SysRole extends BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	@Column(unique = true)
	@NotBlank
    public String name;
	
	@Column(unique = true)
	@NotBlank
    public String code;

	@Transient
	public List<Integer> menuKeys;

	public Integer getId() {
		return id;
	}
}