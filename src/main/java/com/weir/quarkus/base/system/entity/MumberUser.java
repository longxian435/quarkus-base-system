package com.weir.quarkus.base.system.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class MumberUser extends BaseEntity {

	/**自定义id生产*/
	@Id
	@GenericGenerator(name = "my_id", strategy = "com.weir.quarkus.base.system.entity.MyIdGenerator" )
    @GeneratedValue(generator = "my_id")
	public Long id;
	public String name;
	public void setId(Long id) {
		this.id = id;
	}
	
	
}
