package com.weir.quarkus.base.system.vo;

public class VueMenuMetaVo {

	public String icon;
	public String title;
	public VueMenuMetaVo() {
		super();
	}
	public VueMenuMetaVo(String title) {
		super();
		this.title = title;
	}
	public VueMenuMetaVo(String icon, String title) {
		super();
		this.icon = icon;
		this.title = title;
	}
}
