package com.weir.quarkus.base.system.resource;

import com.weir.quarkus.base.system.entity.SysModule;
import com.weir.quarkus.base.system.service.SysModuleService;
import com.weir.quarkus.base.system.vo.*;
//import io.quarkus.qute.Location;
//import io.quarkus.qute.Template;
//import io.quarkus.qute.TemplateInstance;
import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import jakarta.annotation.security.PermitAll;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.util.List;

@Tag(name = "菜单权限管理")
@Path("/sys/module")
@ApplicationScoped
public class SysModuleResource {

	@Inject
	SysModuleService sysModuleService;
	
	@Path("/menu/{id}")
	@DELETE
	public Response delVue(@PathParam("id") Integer id) {
		sysModuleService.del(id);
		return Response.ok().entity(new ResultDataVo<>(200, "ok" ,null)).build();
	}
	@GET
	@Path("menus")
	public Response treeVue(@QueryParam("id") Integer id) {
		List<VueMenuVo> treeVue = sysModuleService.treeVue(id);
		return Response.ok().entity(new ResultDataVo<>(200, "ok" ,treeVue)).build();
	}
//	@RolesAllowed("menu-add")
//	@PermitAll
	@GET
	@Path("menu/list")
	public Response treeList() {
		List<VueTreeVo> treeVue = sysModuleService.allTreeVue();
		return Response.ok().entity(new ResultDataVo<>(200, "ok" ,treeVue)).build();
	}
	@GET
	@Path("menu/list/{userId}")
	public Response treeUserList(@PathParam("userId") Integer userId) {
		List<VueTreeVo> treeVue = sysModuleService.allUserTreeVue(userId);
		return Response.ok().entity(new ResultDataVo<>(200, "ok" ,treeVue)).build();
	}
	@GET
	@Path("tree")
//	@RolesAllowed("module_tree")
	public List<MenuVo> tree(@QueryParam("id") Integer id) {
		return sysModuleService.tree(id);
	}
	
	@GET
	@Path("allTree")
	public List<MenuVo> allTree() {
		return sysModuleService.allTree();
	}
	
	@POST
	@Path("menu")
	@Transactional
	public Response addMenu(AddVueMenuVo module) {
		SysModule tmenu = null;
		if (module.id == null || module.id <= 0) {
			tmenu = new SysModule();
			tmenu.code = module.key;
			tmenu.component = module.component;
			tmenu.name = module.label;
			tmenu.path = module.path;
			tmenu.icon = module.icon;
			tmenu.enable = module.enable;
			tmenu.parentId = module.pId;
			tmenu.stepType = module.type;
            tmenu.persist();
		} else {
			tmenu = SysModule.findById(Integer.valueOf(module.id));
			if (tmenu != null) {
				tmenu.code = module.key;
				tmenu.component = module.component;
				tmenu.name = module.label;
				tmenu.path = module.path;
				tmenu.icon = module.icon;
				tmenu.enable = module.enable;
				tmenu.stepType = module.type;
			}
		}
		return Response.ok().entity(new ResultDataVo<>(200, "ok" ,null)).build();
	}
	@POST
	@Transactional
	public Response add(SysModule module) {
		SysModule tmenu = null;
		if (module.id == null || module.id <= 0) {
			tmenu = new SysModule();
			tmenu = module;
			tmenu.persist();
		} else {
			tmenu = SysModule.findById(Integer.valueOf(module.id));
			if (tmenu != null) {
				tmenu.name = module.name;
				tmenu.code = module.code;
				tmenu.url = module.url;
				tmenu.stepType = module.stepType;
				tmenu.path = module.path;
				tmenu.component = module.component;
			}
		}
		return Response.status(Status.OK).entity(new JsonObject().put("data", tmenu)).build();
	}

//	@Location("admin/menuAdd.html")
//	Template menuAdd;
//
//	@GET
//	@Path("addUI")
//	@Produces(MediaType.TEXT_HTML)
//	public TemplateInstance menuAdd() {
//		return menuAdd.data("tmenu", new SysModule());
//	}
//
//	@Location("admin/menuAdd.html")
//	Template menuEdit;
//
//	@GET
//	@Path("editUI/{id}")
//	@Produces(MediaType.TEXT_HTML)
//	public TemplateInstance menuEdit(@PathParam("id") Integer id) {
//		SysModule tmenu = SysModule.findById(id);
//		return menuEdit.data("tmenu", tmenu);
//	}

	@DELETE
	@Path("/{id}")
	@Transactional
	public Response delete(@PathParam("id") Integer id) {
		SysModule.deleteById(id);
		return Response.status(Status.OK).entity(new JsonObject().put("msg", "删除成功")).build();
	}
}
