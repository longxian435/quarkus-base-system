package com.weir.quarkus.base.system.entity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;

/**
 * 
 * @ClassName: SysModule
 * @Description: 后台菜单模块实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Entity
@Table(name = "sys_module")
public class SysModule extends BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	@Column(unique = true)
	@NotBlank
    public String name;
	
	@Column(unique = true)
	@NotBlank
    public String code;
	public String url;
	@Column(name = "parent_id")
	public Integer parentId;
	
	/**
     * 级别(1菜单和2方法功能)
     */
	@Column(name = "step_type")
	public Integer stepType = 1;
	
	/**
     * 路由
     */
	public String path;

    /**
     * 组件
     */
	public String component;
	public String icon;
	
	public Boolean enable;

	public String getCode() {
		return code;
	}

}