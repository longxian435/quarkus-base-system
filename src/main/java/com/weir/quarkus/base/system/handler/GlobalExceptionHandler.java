package com.weir.quarkus.base.system.handler;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import io.vertx.core.json.JsonObject;

@Provider
public class GlobalExceptionHandler implements ExceptionMapper<Exception> {
    
    @Override
    public Response toResponse(Exception e) {
    	System.out.println("------------------error-------------------" + e.getMessage());
        return Response.status(Response.Status.BAD_REQUEST).entity(new JsonObject().put("message", e.getMessage())).build();
    }
    
}
