package com.weir.quarkus.base.system.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;

import com.weir.quarkus.base.system.entity.SysModule;
import com.weir.quarkus.base.system.entity.SysRole;
import com.weir.quarkus.base.system.entity.SysRoleModule;
import com.weir.quarkus.base.system.entity.SysUser;
import com.weir.quarkus.base.system.entity.SysUserRole;
import com.weir.quarkus.base.system.utils.AesEncyptUtil;

@ApplicationScoped
public class LoginService {

	public void roles(Integer userId) {
		
	}
	
	@Transactional
	public void init() {
		SysModule module = new SysModule();
		module.code = "system";
		module.component = "system";
		module.name = "系统管理";
		module.path = "sys";
		module.stepType = 1;
		SysModule.persist(module);
		
		SysModule module_user = new SysModule();
		module_user.code = "user";
		module_user.component = "sys_user";
		module_user.name = "后台用户管理";
		module_user.path = "user";
		module_user.stepType = 1;
		SysModule.persist(module_user);
		
		SysModule module_module = new SysModule();
		module_module.code = "module";
		module_module.component = "sys_module";
		module_module.name = "后台菜单管理";
		module_module.path = "module";
		module_module.stepType = 1;
		SysModule.persist(module_module);
		
		SysRole role = new SysRole();
		role.code = "admin";
		role.name = "管理员";
		SysRole.persist(role);
		
		SysRoleModule roleModule = new SysRoleModule();
		roleModule.roleId = role.id;
		roleModule.moduleId = module.id;
		SysRoleModule.persist(roleModule);
		SysRoleModule roleModule2 = new SysRoleModule();
		roleModule2.roleId = role.id;
		roleModule2.moduleId = module_user.id;
		SysRoleModule.persist(roleModule2);
		SysRoleModule roleModule3 = new SysRoleModule();
		roleModule3.roleId = role.id;
		roleModule3.moduleId = module_module.id;
		SysRoleModule.persist(roleModule3);
		
		SysUser user = new SysUser();
		user.email = "22222@qq.com";
		user.userName = "weiwei";
		user.userPwd = AesEncyptUtil.encrypt("123456");
		SysUser.persist(user);
		
		SysUserRole userRole = new SysUserRole();
		userRole.userId = user.id;
		userRole.roleId = role.id;
		SysUserRole.persist(userRole);
	}
}
