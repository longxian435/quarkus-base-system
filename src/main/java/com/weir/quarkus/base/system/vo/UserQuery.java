package com.weir.quarkus.base.system.vo;

import com.weir.quarkus.base.system.utils.Query;

import jakarta.ws.rs.QueryParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UserQuery extends BaseQuery {

	@QueryParam(value = "id")
	private Integer id;
	
	// and,like  test is ok
//	@QueryParam(value = "email")
//	@Query(type = Query.Type.INNER_LIKE)
//	private String email;
//	@QueryParam(value = "userName")
//	@Query(type = Query.Type.INNER_LIKE)
//	private String userName;
	
	@QueryParam(value = "email")
	@Query()
	private String email;
	@QueryParam(value = "userName")
	@Query(blurry = "email,userName")
	private String userName;
	
	
	@QueryParam(value = "userPwd")
	private String userPwd;
	@QueryParam(value = "enable")
	private Boolean enable;
	
	@QueryParam(value = "departId")
	@Query(propName = "id", joinName = "depart")
	private Integer departId;
}
