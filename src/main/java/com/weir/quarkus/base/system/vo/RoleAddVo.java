package com.weir.quarkus.base.system.vo;

public class RoleAddVo {

	public Integer id;
	public String code;
	public String name;
	public String moduleIds;
	public String ids;
	public String remark;
}
