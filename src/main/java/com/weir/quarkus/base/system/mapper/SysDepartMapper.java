package com.weir.quarkus.base.system.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.weir.quarkus.base.system.entity.SysDepart;
import com.weir.quarkus.base.system.vo.SysDepartVo;

@Mapper(componentModel = "jakarta", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
//@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SysDepartMapper {

	SysDepartVo toDTO(SysDepart depart);
	SysDepart toDAO(SysDepartVo departVo);
	
	void merge(@MappingTarget SysDepart target,SysDepartVo departVo);
}
