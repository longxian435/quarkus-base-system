package com.weir.quarkus.base.system.resource;

import com.weir.quarkus.base.system.entity.SysModule;
import com.weir.quarkus.base.system.entity.SysUser;
import com.weir.quarkus.base.system.jwt.TokenService;
import com.weir.quarkus.base.system.service.LoginService;
import com.weir.quarkus.base.system.utils.AesEncyptUtil;
import com.weir.quarkus.base.system.vo.*;
import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import jakarta.annotation.security.PermitAll;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * 登录生成token
 * 
 * @author weir
 *
 */
@Tag(name = "登录管理")
@Path("/sys")
@ApplicationScoped
public class LoginResource {

	@Inject
	TokenService tokenService;
	@Inject
	LoginService loginService;

	@GET
	@Path("init")
	public String init() {
		loginService.init();
		return "ok";
	}
	@PermitAll
	@Operation(summary = "登录接口")
	@POST
	@Path("login")
	public Response login(@RequestBody LoginUserVo user) {
		SysUser u = SysUser.find("userName", user.username).firstResult();
		if (u == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultVo<>(400, "用户不存在")).build();
		}
		List<SysModule> modules = SysModule
				.find("select m from SysModule m inner join SysRoleModule rm on m.id = rm.moduleId "
						+ "INNER JOIN SysUserRole ur on rm.roleId=ur.roleId where ur.userId = ?1", u.id)
				.list();
		if (!AesEncyptUtil.encrypt(user.password).equals(u.userPwd)) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultVo<>(400, "用户名或密码错误")).build();
		}
		Set<String> codes = modules.stream().map(SysModule::getCode).collect(Collectors.toSet());
		u.moduleCodes = codes;
//		System.out.println("code----------------------" + codes);
		String ujson = new JsonObject().put("user", u).toString();
		String token = tokenService.generateToken(u.email, ujson, codes);
		u.token = token;
//		System.out.println("token----------------------" + token);
//		String token = null;
		return Response.ok().entity(new ResultDataVo<>(200, "登录成功" , u)).build();
	}
	
//	public static void main(String[] args) {
//		System.out.println(AesEncyptUtil.encrypt("123456"));
//	}
	
	@GET
	@Path("admin_info")
	public Response userInfo() {
		List<PermissionsVo> permissionsVos = new ArrayList<>();
		permissionsVos.add(new PermissionsVo("主控台", "dashboard_console"));
		permissionsVos.add(new PermissionsVo("监控页", "dashboard_monitor"));
		permissionsVos.add(new PermissionsVo("工作台", "dashboard_workplace"));
		permissionsVos.add(new PermissionsVo("基础列表", "basic_list"));
		UserInfoVo userInfoVo = new UserInfoVo();
		userInfoVo.permissions = permissionsVos;
		return Response.ok().entity(new ResultDataVo<>(200, "ok" ,userInfoVo)).build();
//		return userInfoVo;
	}
}
