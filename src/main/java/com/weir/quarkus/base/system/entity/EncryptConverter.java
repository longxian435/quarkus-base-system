package com.weir.quarkus.base.system.entity;

import jakarta.persistence.AttributeConverter;

import com.weir.quarkus.base.system.utils.AesEncyptUtil;

/**
 * 
 * @ClassName: EncryptConverter
 * @Description: 密码加密加密
 * @author weir
 * @date 2021年9月20日
 *
 */
public class EncryptConverter implements AttributeConverter<String, String> {
    /**
     * 加密.
     */
    @Override
    public String convertToDatabaseColumn(String text) {
      return AesEncyptUtil.encrypt(text);
    }

    /**
     * 解密.
     */
    @Override
    public String convertToEntityAttribute(String s) {
      return AesEncyptUtil.desEncrypt(s);
    }
  }