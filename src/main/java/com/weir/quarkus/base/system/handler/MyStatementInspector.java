package com.weir.quarkus.base.system.handler;

import org.hibernate.resource.jdbc.spi.StatementInspector;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MyStatementInspector implements StatementInspector {

	@Override
	public String inspect(String sql) {
		System.out.println("-----------------------------------sql -----------"+sql);
		return null;
	}

}
