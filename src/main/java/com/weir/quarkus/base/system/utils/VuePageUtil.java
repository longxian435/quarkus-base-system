package com.weir.quarkus.base.system.utils;

import jakarta.persistence.TypedQuery;

import com.weir.quarkus.base.system.vo.ElementPageVo;
import com.weir.quarkus.base.system.vo.VuePageListVo;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;

/**
 * vue分页数据计算组装
 * @author weir
 *
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class VuePageUtil {

	public static VuePageListVo toPage(PanacheQuery<PanacheEntityBase> panacheQuery, VuePageListVo list, Integer page,
			Integer pageSize) {
		long count = panacheQuery.count();
		list.page = page;
		list.pageSize = pageSize;
		list.list = panacheQuery.list();
		list.pageCount = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;
		
		ElementPageVo pageVo = new ElementPageVo();
		pageVo.page = page;
		pageVo.pageSize = pageSize;
		pageVo.total = count;
		list.pageEl = pageVo;
		return list;
	}
	public static VuePageListVo toPageForTypedQuery(TypedQuery typedQuery, VuePageListVo list, Integer page,
			Integer pageSize) {
		Long count = Integer.valueOf(typedQuery.getResultList().size()).longValue();
		list.page = page;
		list.pageSize = pageSize;
		list.list = typedQuery.setFirstResult(page * pageSize).setMaxResults(pageSize).getResultList();
		list.pageCount = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;
		
		ElementPageVo pageVo = new ElementPageVo();
		pageVo.page = page;
		pageVo.pageSize = pageSize;
		pageVo.total = count;
		list.pageEl = pageVo;
		return list;
	}
	public static VuePageListVo toListForTypedQuery(TypedQuery typedQuery, VuePageListVo list) {
		list.list = typedQuery.getResultList();
		return list;
	}
}
