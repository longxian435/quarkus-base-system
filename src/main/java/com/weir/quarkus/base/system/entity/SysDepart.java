package com.weir.quarkus.base.system.entity;

import java.io.Serializable;

import jakarta.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 部门
 * @author weir
 * @date 2023-03-17 02:13:23
 */
@Data
@Entity
@Table(name = "sys_depart")
//@NamedEntityGraph(name = "SysDepart.Graph", attributeNodes = {@NamedAttributeNode("children")})
//@NamedEntityGraphs(
//        @NamedEntityGraph(name = "SysDepart.findAll",
//                attributeNodes = {
//                @NamedAttributeNode("children")
//        })
//)
@EqualsAndHashCode(callSuper=false)
public class SysDepart extends BaseDataEntity implements Serializable {
    
	private static final long serialVersionUID = -8071945109829724868L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	/**父机构ID*/
//	@ManyToOne
//    @JoinColumn(name="parent_id")
//	private SysDepart parent;
	
	@Column(name = "parent_id")
	private Integer parent;
	/**机构/部门名称*/
	private String name;
	/**英文名*/
	@Column(name = "name_en")
	private String nameEn;
	/**排序*/
	private Integer sort;
	/**描述*/
	private String description;
	/**机构类别 1=公司，2=组织机构，3=岗位*/
	@Column(name = "org_category")
	private Integer orgCategory;
	/**机构编码*/
	@Column(name = "org_code")
	private String orgCode;
	/**手机号*/
	private String mobile;
	/**传真*/
	private String fax;
	/**地址*/
	private String address;
	/**状态（1启用，0不启用）*/
	private Boolean status;
	/**删除状态（0，正常，1已删除）*/
	@Column(name = "del_flag")
	private Boolean delFlag;

//	@OneToMany(cascade=CascadeType.ALL,mappedBy = "parent")
//    @JoinColumn(name="parent_id")
//    private Set<SysDepart> children;
	
}
