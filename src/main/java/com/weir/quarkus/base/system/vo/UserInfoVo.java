package com.weir.quarkus.base.system.vo;

import java.util.List;

public class UserInfoVo {
    public Integer userId = 1;
    public String username = "admin";
    public String realName = "Admin";
    public String avatar;
    public String desc = "manager";
    public String password;
    public String token;
    public List<PermissionsVo> permissions;
}
