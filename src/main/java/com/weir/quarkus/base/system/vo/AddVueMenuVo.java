package com.weir.quarkus.base.system.vo;


public class AddVueMenuVo {

	public Integer id;
	public String label;
	public String key;
	public Integer type = 1;
	public String subtitle;
	public Integer openType = 1;
	public String auth;
	public String component;
	public String path;
	public String icon;
	public Integer pId;
	public Boolean enable;
	
}
