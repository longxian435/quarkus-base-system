package com.weir.quarkus.base.system.resource;

import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.base.system.entity.MumberUser;

import io.vertx.core.json.JsonObject;

@Tag(name = "测试自定义ID")
@Path("/sys/mumber_user")
@ApplicationScoped
public class MumberUserResource {

	@Inject
	EntityManager em;
	
	@POST
	@Path("role")
	@Transactional
	public Response add(MumberUser mumberUser) {
		mumberUser.persist();
		return Response.status(Status.OK).entity(new JsonObject().put("msg", "添加或修改成功").put("mumberUser", mumberUser)).build();
	}
	
	@RolesAllowed("user_list")
	@POST
	@Path("save-or-update")
	@Transactional
	public Response saveOrUpdate(MumberUser mumberUser) {
		em.merge(mumberUser);
		return Response.status(Status.OK).entity(new JsonObject().put("msg", "添加或修改成功").put("mumberUser", mumberUser)).build();
	}
}
