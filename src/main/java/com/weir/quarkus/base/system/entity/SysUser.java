package com.weir.quarkus.base.system.entity;
import java.util.List;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotBlank;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import com.weir.quarkus.base.aspect.DictData;

/**
 * 
 * @ClassName: SysUser
 * @Description: 后台用户实体
 * @author weir
 * @date 2021年8月25日
 *
 */
//@FilterDef(name = "orderOwnerFilter", parameters = {@ParamDef(name= "departIds",type = "integer")})
//@Filter(name= "orderOwnerFiler", condition = "departId in (:departIds)")
@Entity
@Table(name = "sys_user")
public class SysUser extends BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	
	// 唯一性
	@DictData(code = "email",table = "temail")
	@Column(unique = true)
	@NotBlank
    public String email;
	
	@DictData(code = "user_name",table = "sys_user",text = "email")
	@Column(unique = true, name = "user_name")
	@NotBlank
    public String userName;
	
	// 密码加密保存和解密展示
//	@Convert(converter = EncryptConverter.class)
	@Column(name = "user_pwd")
	@DictData(code = "sex")
    public String userPwd;
//	@Column(name = "depart_id")
//	public Integer departId;
	@ManyToOne
	@JoinColumn(name="depart_id")
	public SysDepart depart;
	
	public boolean enable = true;
	
	@Transient
	public Set<String> moduleCodes;
	@Transient
	public String token;
	@Transient
	public List<Integer> roleList;

	public Integer getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getUserName() {
		return userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

}