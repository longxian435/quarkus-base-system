package com.weir.quarkus.base.system.vo;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class VueTreeVo {

	public Integer id;
	public String label;
	public String key;
	public Integer type = 1;
	public String subtitle;
	public Integer openType = 1;
	public String auth;
	public String path;
	public String parentPath;
	public String component;
	public String icon;
	public Integer stepType;
	public Boolean enable;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date createTime;
	
	public List<VueTreeVo> children;
}
