//package com.weir.quarkus.base.system.resource;
//
//import io.quarkus.qute.Location;
//import io.quarkus.qute.Template;
//import io.quarkus.qute.TemplateInstance;
//
//import javax.enterprise.context.ApplicationScoped;
//import javax.ws.rs.GET;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//
//@ApplicationScoped
//@Path("admin/index")
//@Produces(MediaType.TEXT_HTML)
//public class IndexResource {
//
//	@Location("admin/north.html")
//    Template north;
//    @Location("admin/south.html")
//    Template south;
//    @Location("admin/welcome.html")
//    Template welcome;
//    @Location("admin/main.html")
//    Template main;
//    @Location("login.html")
//    Template login;
//
//    @Location("admin/menu.html")
//    Template menu;
//    @Location("admin/user.html")
//    Template user;
//    @Location("admin/role.html")
//    Template role;
//
//    @GET
////    @Path("main")
//    public TemplateInstance getMain() {
//        return main.data(null);
//    }
//
//    @GET
//    @Path("login")
//    public TemplateInstance getLogin() {
//        return login.data(null);
//    }
//
//
//    @GET
//    @Path("north")
//    public TemplateInstance getNorth() {
//    	return north.data(null);
//    }
//    @GET
//    @Path("south")
//    public TemplateInstance getSouth() {
//    	return south.data(null);
//    }
//    @GET
//    @Path("welcome")
//    public TemplateInstance getWelcom() {
//    	return welcome.data(null);
//    }
//
//
//	@GET
//    @Path("menu")
//    public TemplateInstance menu() {
//        return menu.data(null);
//    }
//	@GET
//	@Path("user")
//	public TemplateInstance user() {
//		return user.data(null);
//	}
//	@GET
//	@Path("role")
//	public TemplateInstance role() {
//		return role.data(null);
//	}
//
//}
