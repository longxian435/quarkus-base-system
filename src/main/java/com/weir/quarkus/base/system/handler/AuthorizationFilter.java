package com.weir.quarkus.base.system.handler;

import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.RoutingContext;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.ext.Provider;

/**
 *
 * @ClassName: AuthorizationFilter
 * @Description: url权限拦截
 * @author weir
 * @date 2021年8月28日
 *
 */
@ApplicationScoped
@Priority(Priorities.USER + 200)
@Provider
public class AuthorizationFilter implements ContainerRequestFilter {

	@ConfigProperty(name = "authorization.permit-patterns")
	String permitPatterns;
	
    //  RESTEASY003880: Unable to find contextual data of type: io.vertx.core.http.HttpServerRequest
	@Context
	HttpServerRequest request;
	@Inject
	RoutingContext context;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) {
    	System.out.println("----------containerRequestContext----request------------" + request.remoteAddress().host());
    	System.out.println("----------containerRequestContext----context------------" + context.request().remoteAddress().host());
    	
//    	String[] splits = StringUtils.split(permitPatterns, ",");
//    	List<String> list = Arrays.asList(splits);
//    	String path = containerRequestContext.getUriInfo().getPath();
//    	if (list.stream().anyMatch(path::contains)) {
//			return;
//		}
//        Map<String, List<String>> headers = containerRequestContext.getHeaders();
//        List<String> token = headers.get("Authorization");
//        if (token == null || token.isEmpty()) {
////        	throw new RuntimeException("No Authorization");
//		}

//        try {
//			SysUser user = TokenUtils.getUser(token.get(0));
//			if (user == null) {
//				throw new RuntimeException("非法用户");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
    }
//    @Override
//    public void filter(ContainerRequestContext containerRequestContext) {
//    	String[] splits = StringUtils.split(permitPatterns, ",");
//    	List<String> list = Arrays.asList(splits);
//    	String path = containerRequestContext.getUriInfo().getPath();
//    	if (list.stream().anyMatch(path::contains)) {
//    		return;
//    	}
//    	Map<String, Cookie> cookies = containerRequestContext.getCookies();
//    	Cookie cookieToken = cookies.get("weir-token");
//    	if (cookieToken == null) {
//    		try {
//    			containerRequestContext.setRequestUri(new URI("admin/index"));
//    		} catch (URISyntaxException e) {
//    			e.printStackTrace();
//    		}
//    	}
//
//    }


}