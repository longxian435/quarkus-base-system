package com.weir.quarkus.base.system.vo;

import lombok.Data;

/**
 * 部门
 * @author weir
 * @date 2023-03-17 02:13:23
 */
@Data
public class SysDepartVo {
    
	private Integer id;
	/**父机构ID*/
	private Integer parentId;
	/**机构/部门名称*/
	private String name;
	/**英文名*/
	private String nameEn;
	/**排序*/
	private Integer sort;
	/**描述*/
	private String description;
	/**机构类别 1=公司，2=组织机构，3=岗位*/
	private Integer orgCategory;
	/**机构编码*/
	private String orgCode;
	/**手机号*/
	private String mobile;
	/**传真*/
	private String fax;
	/**地址*/
	private String address;
	/**状态（1启用，0不启用）*/
	private Boolean status;
	/**删除状态（0，正常，1已删除）*/
	private Boolean delFlag;

}
