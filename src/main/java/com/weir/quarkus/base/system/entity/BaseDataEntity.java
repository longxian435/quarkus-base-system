package com.weir.quarkus.base.system.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
//import io.quarkus.qute.TemplateData;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.weir.quarkus.base.system.handler.BaseEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @ClassName: BaseEntity
 * @Description: 实体公共基类（表的创建时间 创建人 修改时间 修改人 分页的当前页 每页数据）
 * @author weir
 * @date 2021年8月25日
 *
 */
@MappedSuperclass
public class BaseDataEntity extends PanacheEntityBase {

	@Transient
	private Integer page = 0;
	@Transient
	private Integer rows = 10;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "create_time")
//	@CreationTimestamp
	private Date createTime;

	private Integer creator;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "modify_time")
//	@UpdateTimestamp  //添加也会执行
	private Date modifyTime;

	private Integer modifier;

	private String remark;

}