package com.weir.quarkus.base.system.resource;

import com.weir.quarkus.base.system.entity.SysArea;
import com.weir.quarkus.base.system.entity.SysDepart;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

@Tag(name = "行政区划管理")
@Path("/area")
@ApplicationScoped
public class SysAreaResource {
	
	@GET
	@Path("list/{code}")
	public List<SysDepart> list(@PathParam("code") Long code) {
		return SysArea.list("pcode", code);
	}

}
