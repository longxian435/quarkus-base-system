package com.weir.quarkus.base.system.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import com.alibaba.excel.annotation.ExcelProperty;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Table(name = "sys_area")
public class SysArea extends PanacheEntityBase {

	@ExcelProperty("区划代码")
	@Id
	@GeneratedValue
	public Long code;
	@ExcelProperty("父级区划代码")
	public Long pcode;
	@ExcelProperty("名称")
	public String name;
	@ExcelProperty("级别1-5,省市县镇村")
	public Integer level;
	@ExcelProperty("城乡分类")
	public Integer category;
}
