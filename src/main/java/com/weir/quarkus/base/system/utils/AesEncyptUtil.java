package com.weir.quarkus.base.system.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * 
 * @ClassName: AesEncyptUtil
 * @Description: AES算法进行加密
 * @author weir
 * @date 2021年8月25日
 *
 */
public class AesEncyptUtil {
	/**
	 * AES加密模式
	 */
	private static final String AES_MODE = "AES/CBC/NOPadding";
	/**
	 * AES的iv向量值
	 */
	private static final String AES_IV = "123456789012weir";

	public static final String AES_KEY = "weir123456789876";

	/**
	 * 加密方法
	 * 
	 * @param data 要加密的数据
	 * @param key  加密key
	 * @param iv   加密iv
	 * @return 加密的结果
	 * @throws Exception
	 */
	public static String encrypt(String data, String key, String iv) {
		try {
			Cipher cipher = Cipher.getInstance(AES_MODE);
			int blockSize = cipher.getBlockSize();
			byte[] dataBytes = data.getBytes();
			int plaintextLength = dataBytes.length;
			if (plaintextLength % blockSize != 0) {
				plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
			}
			byte[] plaintext = new byte[plaintextLength];
			System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
			SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
			IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
			cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
			byte[] encrypted = cipher.doFinal(plaintext);
			return new Base64().encodeToString(encrypted);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 解密方法
	 * 
	 * @param data 要解密的数据
	 * @param key  解密key
	 * @param iv   解密iv
	 * @return 解密的结果
	 * @throws Exception
	 */
	public static String desEncrypt(String data, String key, String iv) {
		try {
			byte[] encrypted1 = new Base64().decode(data);
			Cipher cipher = Cipher.getInstance(AES_MODE);
			SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
			IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
			cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
			byte[] original = cipher.doFinal(encrypted1);
			String originalString = new String(original);
			return originalString;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 使用默认的key和iv加密
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String data) {
		return encrypt(data, AES_KEY, AES_IV);
	}

	/**
	 * 使用默认的key和iv解密
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public static String desEncrypt(String data) {
		return desEncrypt(data, AES_KEY, AES_IV);
	}

	public static void main(String[] args) {
		System.out.println(AesEncyptUtil.encrypt("123456"));
		System.out.println(AesEncyptUtil.desEncrypt("DimebHZ8B8RLhcf4bCvhEw=="));
	}
}