package com.weir.quarkus.base.system.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;

/**
 * 字典明细
 * @author weir
 *
 */
@Entity
@Table(name = "sys_dict_item")
public class SysDictItem extends BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	@NotBlank
    public Integer pId;
	
	@NotBlank
    public String itemText;
	@NotBlank
	public String itemValue;

}