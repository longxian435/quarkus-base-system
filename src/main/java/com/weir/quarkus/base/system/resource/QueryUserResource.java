package com.weir.quarkus.base.system.resource;

import com.weir.quarkus.base.system.entity.SysUser;
import com.weir.quarkus.base.system.utils.QueryHelper;
import com.weir.quarkus.base.system.vo.ResultDataVo;
import com.weir.quarkus.base.system.vo.UserQuery;
import com.weir.quarkus.base.system.vo.VuePageListVo;
import io.vertx.core.http.HttpServerRequest;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;

/**
 * 
 * @ClassName: SysUserResource
 * @Description: 后台用户API管理
 * @author weir
 * @date 2021年8月25日
 *
 */
@Tag(name = "用户查询测试")
@Path("/query")
@ApplicationScoped
public class QueryUserResource {

	@Context
	HttpServerRequest request;
	@Inject
	EntityManager em;

	@GET
	@Path("user/list")
	public Response querylist(@BeanParam UserQuery query) {
		VuePageListVo<SysUser> queryPage = QueryHelper.createQueryPage(em, SysUser.class, query);
		return Response.ok().entity(new ResultDataVo<>(200, "ok", queryPage)).build();
	}

}
