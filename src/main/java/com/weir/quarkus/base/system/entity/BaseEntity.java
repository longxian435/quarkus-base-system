package com.weir.quarkus.base.system.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.weir.quarkus.base.system.handler.BaseEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;
import java.util.Date;

/**
 * 
 * @ClassName: BaseEntity
 * @Description: 实体公共基类（表的创建时间 创建人 修改时间 修改人 分页的当前页 每页数据）
 * @author weir
 * @date 2021年8月25日
 *
 */
@MappedSuperclass
@EntityListeners(BaseEntityListener.class)
public class BaseEntity extends PanacheEntityBase {

	@Transient
	public Integer page = 0;
	@Transient
	public Integer rows = 10;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "create_time")
//	@CreationTimestamp
	public Date createTime;

	public Integer creator;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "modify_time")
//	@UpdateTimestamp  //添加也会执行
	public Date modifyTime;

	public Integer modifier;

	public String remark;

}