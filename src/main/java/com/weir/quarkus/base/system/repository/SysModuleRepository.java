package com.weir.quarkus.base.system.repository;

import jakarta.enterprise.context.ApplicationScoped;

import com.weir.quarkus.base.system.entity.SysModule;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class SysModuleRepository implements PanacheRepository<SysModule> {

}
